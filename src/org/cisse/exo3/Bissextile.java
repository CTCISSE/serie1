package org.cisse.exo3;

public class Bissextile {

	public boolean bissextile(int i)
	{
		if(i%4 == 0 &&( i%100>0 || i%400==0) )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}
