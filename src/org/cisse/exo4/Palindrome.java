package org.cisse.exo4;

public class Palindrome {

	public boolean palindrome(String mot)
	{
		int taille;
		int i;
		int j;
		mot = mot.replace(" ", "");
		taille = mot.length();
		
		mot = mot.toUpperCase();
		
		for(i=0; i<(int)(taille/2);i++)
		{
			
				if(mot.charAt(i) != mot.charAt(taille-1-i))
				{
					return false;
				}

		}
		
		
		return true;
	}
}
