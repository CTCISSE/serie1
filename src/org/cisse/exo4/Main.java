package org.cisse.exo4;

public class Main {
	
	public static void main(String[] args)
	{
		String mot = "je demande dej";
		
		Palindrome phrase = new Palindrome();
		
		if(phrase.palindrome(mot) == true)
		{
			System.out.println(mot + " est un palindrome");
		}
		else
		{
			System.out.println(mot + " n'est pas un palindrome");
		}
		
		
	}

}
