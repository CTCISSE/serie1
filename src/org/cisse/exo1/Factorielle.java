package org.cisse.exo1;

import java.math.*;

public class Factorielle {


	public int intFactorielle(int i)
	{
		if(i == 1)
		{
			return 1;
		}
		else
		{
			return i*intFactorielle(i-1);
		}

	}
	
	public double doubleFactorielle(int i)
	{
		if(i==1)
		{
			return 1;
		}
		else
		{
			return i*doubleFactorielle(i-1);
		}
	}
	
	
	public BigInteger BigIntFactorielle(int i)
	{
		if(i==1)
		{
			return BigInteger.valueOf(1);
		}
		else
		{
			return BigIntFactorielle(i-1).multiply(BigInteger.valueOf(i));
		}
	}
	
	public BigInteger factorielle(int i)
	{
		if (i> 0 & i<13)
		{
			return BigInteger.valueOf(this.intFactorielle(i));
		}
		else if (i>= 13 && i <21)
		{
			return BigInteger.valueOf((long) this.doubleFactorielle(i));
		}
		else
		{
			return this.BigIntFactorielle(i);
		
		}
	}
	
	
}
